package base;
import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Instrumento {

    private ObjectId id;
    private String codigo;
    private String tipo;
    private String nombre;
    private double pvp;
    private LocalDate fecha;

    public Instrumento(){

    }

    public Instrumento(String codigo, String tipo, String nombre, double pvp, LocalDate fecha) {
        this.codigo = codigo;
        this.tipo = tipo;
        this.nombre = nombre;
        this.pvp = pvp;
        this.fecha = fecha;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPvp() {
        return pvp;
    }

    public void setPvp(double pvp) {
        this.pvp = pvp;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return codigo+" "+tipo+" "+nombre+" "+pvp+" "+fecha;
    }
}
