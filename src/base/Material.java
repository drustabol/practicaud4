package base;

import org.bson.types.ObjectId;

public class Material {
    private ObjectId id;
    private String nombre;
    private String calidad;
    private String organico;
    private String procedencia;

    public Material(){

    }

    public Material(String nombre, String calidad, String organico, String procedencia) {
        this.nombre = nombre;
        this.calidad = calidad;
        this.organico = organico;
        this.procedencia = procedencia;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCalidad() {
        return calidad;
    }

    public void setCalidad(String calidad) {
        this.calidad = calidad;
    }

    public String getOrganico() {
        return organico;
    }

    public void setOrganico(String organico) {
        this.organico = organico;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String toString() {
        return nombre+" "+calidad+" "+organico+" "+procedencia;
    }
}
