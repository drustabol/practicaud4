package base;

import org.bson.types.ObjectId;

public class Marca {
    ObjectId id;
    String nombre;
    String pais;
    String delegacion;
    String web;

    public Marca(){

    }

    public Marca(String nombre, String pais, String delegacion, String web) {
        this.nombre = nombre;
        this.pais = pais;
        this.delegacion = delegacion;
        this.web = web;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDelegacion() {
        return delegacion;
    }

    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Override
    public String toString() {
        return nombre+" "+ pais +" "+delegacion+" "+web;
    }
}
