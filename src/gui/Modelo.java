package gui;

import base.Instrumento;
import base.Marca;
import base.Material;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Modelo {
    private MongoClient cliente;
    private MongoCollection<Document> instrumentos;
    private MongoCollection<Document> marcas;
    private MongoCollection<Document> materiales;

    public void conectar() {
        cliente = new MongoClient();
        String DATABASE = "bInstrumentos";
        MongoDatabase db = cliente.getDatabase(DATABASE);

        String COLECCION_INSTRUMENTOS = "Instrumentos";
        instrumentos = db.getCollection(COLECCION_INSTRUMENTOS);
        String COLECCION_MARCAS = "Marcas";
        marcas = db.getCollection(COLECCION_MARCAS);
        String COLECCION_MATERIALES = "Materiales";
        materiales = db.getCollection(COLECCION_MATERIALES);
    }

    public void desconectar() {
        cliente.close();
        cliente = null;
    }

    public MongoClient getCliente() {
        return cliente;
    }

    public ArrayList<Instrumento> getInstrumentos() {
        ArrayList<Instrumento> lista = new ArrayList<>();

        for (Document document : instrumentos.find()) {
            lista.add(documentToInstrumento(document));
        }
        return lista;
    }


    public ArrayList<Instrumento> getInstrumentos(String comparador) {

        ArrayList<Instrumento> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("codigo", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : instrumentos.find(query)) {
            lista.add(documentToInstrumento(document));
        }

        return lista;
    }

    public ArrayList<Marca> getMarcas() {
        ArrayList<Marca> lista = new ArrayList<>();

        for (Document document : marcas.find()) {
            lista.add(documentToMarca(document));
        }
        return lista;
    }

    public ArrayList<Marca> getMarcas(String comparador) {

        ArrayList<Marca> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : marcas.find(query)) {
            lista.add(documentToMarca(document));
        }

        return lista;
    }

    public ArrayList<Material> getMateriales() {
        ArrayList<Material> lista = new ArrayList<>();

        for (Document document : materiales.find()) {
            lista.add(documentToMaterial(document));
        }
        return lista;
    }

    public ArrayList<Material> getMateriales(String comparador) {

        ArrayList<Material> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : materiales.find(query)) {
            lista.add(documentToMaterial(document));
        }

        return lista;
    }

    public void guardarObjeto(Object obj) {
        if (obj instanceof Instrumento) {
            instrumentos.insertOne(objectToDocument(obj));
        } else if (obj instanceof Marca) {
            marcas.insertOne(objectToDocument(obj));
        } else if (obj instanceof Material) {
            materiales.insertOne(objectToDocument(obj));
        }
    }

    public void modificarObjeto(Object obj) {
        if (obj instanceof Instrumento) {
            Instrumento instrumento = (Instrumento) obj;
            instrumentos.replaceOne(new Document("_id", instrumento.getId()), objectToDocument(instrumento));
        } else if (obj instanceof Marca) {
            Marca marca = (Marca) obj;
            marcas.replaceOne(new Document("_id", marca.getId()), objectToDocument(marca));
        } else if (obj instanceof Material) {
            Material material = (Material) obj;
            materiales.replaceOne(new Document("_id", material.getId()), objectToDocument(material));
        }
    }

    public void eliminarObjeto(Object obj) {
        if (obj instanceof Instrumento) {
            Instrumento instrumento = (Instrumento) obj;
            instrumentos.deleteOne(objectToDocument(instrumento));
        } else if (obj instanceof Marca) {
            Marca marca = (Marca) obj;
            marcas.deleteOne(objectToDocument(marca));
        } else if (obj instanceof Material) {
            Material material = (Material) obj;
            materiales.deleteOne(objectToDocument(material));
        }
    }

    private Marca documentToMarca(Document document) {
        Marca marca = new Marca();
        marca.setId(document.getObjectId("_id"));
        marca.setNombre(document.getString("nombre"));
        marca.setPais(document.getString("pais"));
        marca.setDelegacion(document.getString("delegacion"));
        marca.setWeb(document.getString("web"));
        return marca;
    }

    private Instrumento documentToInstrumento(Document document) {
        Instrumento instrumento = new Instrumento();
        instrumento.setId(document.getObjectId("_id"));
        instrumento.setCodigo(document.getString("codigo"));
        instrumento.setTipo(document.getString("tipo"));
        instrumento.setNombre(document.getString("nombre"));
        instrumento.setPvp(document.getDouble("pvp"));
        instrumento.setFecha(LocalDate.parse(document.getString("fecha")));

        return instrumento;
    }

    private Material documentToMaterial(Document document) {
        Material material = new Material();
        material.setId(document.getObjectId("_id"));
        material.setNombre(document.getString("nombre"));
        material.setCalidad(document.getString("calidad"));
        material.setOrganico(document.getString("organico"));
        material.setProcedencia(document.getString("procedencia"));
        return material;
    }

    public Document objectToDocument(Object obj) {
        Document document = new Document();

        if (obj instanceof Instrumento) {
            Instrumento instrumento = (Instrumento) obj;

            document.append("codigo", instrumento.getCodigo());
            document.append("tipo", instrumento.getTipo());
            document.append("nombre", instrumento.getNombre());
            document.append("pvp", instrumento.getPvp());
            document.append("fecha", instrumento.getFecha().toString());
        } else if (obj instanceof Marca) {
            Marca marca = (Marca) obj;

            document.append("nombre", marca.getNombre());
            document.append("pais", marca.getPais());
            document.append("delegacion", marca.getDelegacion());
            document.append("web", marca.getWeb());

        } else if (obj instanceof Material) {
            Material material = (Material) obj;
            document.append("nombre", material.getNombre());
            document.append("calidad", material.getCalidad());
            document.append("organico", material.getOrganico());
            document.append("procedencia", material.getProcedencia());
        } else {
            return null;
        }
        return document;
    }
}
