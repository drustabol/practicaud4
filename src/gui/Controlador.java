package gui;

import base.Instrumento;
import base.Marca;
import base.Material;
import util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    private Modelo modelo;
    private Vista vista;

    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);

        try {
            modelo.conectar();
            vista.itemConectar.setText("Desconectar");
            listarInstrumentos();
            listarMarcas();
            listarMateriales();
        } catch (Exception ex) {
            Util.showErrorAlert("Imposible establecer conexión con el servidor.");
        }
    }

    private void addListSelectionListeners(ListSelectionListener listener) {
        vista.listInstrumentos.addListSelectionListener(listener);
        vista.listMarcas.addListSelectionListener(listener);
        vista.listMaterial.addListSelectionListener(listener);
    }

    private void addKeyListeners(KeyListener listener) {
        vista.txtBuscarInstrumento.addKeyListener(listener);
        vista.txtBuscarMarca.addKeyListener(listener);
        vista.txtBuscarMaterial.addKeyListener(listener);
    }

    private void addActionListeners(ActionListener listener) {
        vista.addInstrumentoBtn.addActionListener(listener);
        vista.addInstrumentoBtn.setActionCommand("addInstrumentoBtn");
        vista.modificarInstrumentoBtn.addActionListener(listener);
        vista.modificarInstrumentoBtn.setActionCommand("modificarInstrumentoBtn");
        vista.eliminarInstrumentoBtn.addActionListener(listener);
        vista.eliminarInstrumentoBtn.setActionCommand("eliminarInstrumentoBtn");

        vista.addMarcaBtn.addActionListener(listener);
        vista.addMarcaBtn.setActionCommand("addMarcaBtn");
        vista.modificarMarcaBtn.addActionListener(listener);
        vista.modificarMarcaBtn.setActionCommand("modificarMarcaBtn");
        vista.eliminarMarcaBtn.addActionListener(listener);
        vista.eliminarMarcaBtn.setActionCommand("eliminarMarcaBtn");

        vista.addMaterialBtn.addActionListener(listener);
        vista.addMaterialBtn.setActionCommand("addMaterialBtn");
        vista.modificarMaterialBtn.addActionListener(listener);
        vista.modificarMaterialBtn.setActionCommand("modificarMaterialBtn");
        vista.eliminarMaterialBtn.addActionListener(listener);
        vista.eliminarMaterialBtn.setActionCommand("eliminarMaterialBtn");

        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "conexion":
                try {
                    if (modelo.getCliente() == null) {
                        modelo.conectar();
                        listarInstrumentos();
                        listarMarcas();
                        listarMateriales();
                    } else {
                        modelo.desconectar();
                        vista.dlmInstrumentos.clear();
                        vista.dlmMarcas.clear();
                        vista.dlmMateriales.clear();
                        limpiarCamposInstrumento();
                        limpiarCamposMarca();
                        limpiarCamposMaterial();
                    }
                } catch (Exception ex) {
                    Util.showErrorAlert("Imposible establecer conexión con el servidor.");
                }
                break;

            case "salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "addInstrumentoBtn":
                if (comprobarCamposInstrumento()) {
                    try {
                        modelo.guardarObjeto(new Instrumento(vista.txtCodigo.getText(),
                                String.valueOf(vista.comboTipo.getSelectedItem()),
                                vista.txtNombreInstrumento.getText(),
                                Double.parseDouble(vista.txtPvp.getText()),
                                vista.datePicker.getDate()));
                    } catch (NumberFormatException ex) {
                        Util.showErrorAlert("El campo de precio sólo admite números");
                    }
                    limpiarCamposInstrumento();
                } else {
                    Util.showErrorAlert("Hay que rellenar todos los campos");
                }
                listarInstrumentos();
                break;

            case "modificarInstrumentoBtn":

                if (vista.listInstrumentos.getSelectedValue() != null) {
                    if (comprobarCamposInstrumento()) {
                        Instrumento instrumentoSelecionado = (Instrumento) vista.listInstrumentos.getSelectedValue();
                        instrumentoSelecionado.setCodigo(vista.txtCodigo.getText());
                        instrumentoSelecionado.setTipo(String.valueOf(vista.comboTipo.getSelectedItem()));
                        instrumentoSelecionado.setNombre(vista.txtNombreInstrumento.getText());

                        try {
                            instrumentoSelecionado.setPvp(Double.parseDouble(vista.txtPvp.getText()));
                        } catch (NumberFormatException e1) {
                            Util.showErrorAlert("El campo de precio sólo admite números");
                        }
                        instrumentoSelecionado.setFecha(vista.datePicker.getDate());
                        modelo.modificarObjeto(instrumentoSelecionado);
                    } else {
                        Util.showErrorAlert("No hay nada seleccionado que modificar");
                    }
                }
                break;

            case "eliminarInstrumentoBtn":
                if (vista.listInstrumentos.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listInstrumentos.getSelectedValue());
                } else {
                    Util.showErrorAlert("No hay nada seleccionado que borrar");
                }
                break;

            case "addMarcaBtn":
                if (comprobarCamposMarca()) {
                    modelo.guardarObjeto(new Marca(vista.txtNombreMarca.getText(),
                            String.valueOf(vista.comboPais.getSelectedItem()),
                            vista.txtDelegacion.getText(),
                            vista.txtWeb.getText()));
                } else {
                    Util.showErrorAlert("Rellenar todos los campos");
                }
                break;

            case "modificarMarcaBtn":
                if (vista.listMarcas.getSelectedValue() != null) {
                    if (comprobarCamposMarca()) {
                        Marca marcaSeleccionada = (Marca) vista.listMarcas.getSelectedValue();
                        marcaSeleccionada.setNombre(vista.txtNombreMarca.getText());
                        marcaSeleccionada.setPais(String.valueOf(vista.comboPais.getSelectedItem()));
                        marcaSeleccionada.setDelegacion(vista.txtDelegacion.getText());
                        marcaSeleccionada.setWeb(vista.txtWeb.getText());
                        modelo.modificarObjeto(marcaSeleccionada);
                    }
                } else {
                    Util.showErrorAlert("No hay nada seleccionado que modificar");
                }
                break;

            case "eliminarMarcaBtn":
                if (vista.listMarcas.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listMarcas.getSelectedValue());
                } else {
                    Util.showErrorAlert("No hay nada seleccionado que borrar");
                }

                break;


            case "addMaterialBtn":
                if (vista.rBtnSi.isSelected()) {

                    vista.setOrganico("Si");
                }
                if (vista.rBtnNo.isSelected()) {

                    vista.setOrganico("No");
                }

                if (comprobarCamposMaterial()) {
                    Util.showErrorAlert("Rellena todos los campos");

                } else {


                    modelo.guardarObjeto(new Material(vista.txtNombreMaterial.getText(),
                            String.valueOf(vista.comboCalidad.getSelectedItem()),
                            vista.getOrganico(),
                            vista.txtProcedencia.getText()));
                }
                break;

            case "modificarMaterialBtn":

                if (vista.listMaterial.getSelectedValue() != null) {


                   if (comprobarCamposMarca()) {
                        Material materialSeleccionado = (Material) vista.listMaterial.getSelectedValue();
                        materialSeleccionado.setNombre(vista.txtNombreMaterial.getText());
                        materialSeleccionado.setCalidad(String.valueOf(vista.comboCalidad.getSelectedItem()));
                        System.out.println(String.valueOf(vista.comboCalidad.getSelectedItem()));
                    if (vista.rBtnSi.isSelected()) {

                        vista.setOrganico("Si");
                    }
                    if (vista.rBtnNo.isSelected()) {

                        vista.setOrganico("No");
                    }
                        materialSeleccionado.setProcedencia(vista.txtProcedencia.getText());
                        modelo.modificarObjeto(materialSeleccionado);
                   }
                } else {
                    Util.showErrorAlert("No hay nada seleccionado que modificar");
                }
                break;

            case "eliminarMaterialBtn":
                if (vista.listMaterial.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listMaterial.getSelectedValue());
                } else {
                    Util.showErrorAlert("No hay nada seleccionado que borrar");
                }

                break;

        }
        limpiarTodo();
        listarTodo();
    }

    private void limpiarTodo() {
        limpiarCamposInstrumento();
        limpiarCamposMarca();
        limpiarCamposMaterial();
    }

    private void listarTodo() {
        listarInstrumentos();
        listarMarcas();
        listarMateriales();
    }

    private void limpiarCamposInstrumento() {
        vista.txtCodigo.setText("");
        vista.comboTipo.setSelectedIndex(-1);
        vista.txtNombreInstrumento.setText("");
        vista.txtPvp.setText("");
        vista.datePicker.setDate(null);
    }

    private void limpiarCamposMarca() {
        vista.txtNombreMarca.setText("");
        vista.comboPais.setSelectedItem(-1);
        vista.txtDelegacion.setText("");
        vista.txtWeb.setText("");
    }

    private void limpiarCamposMaterial() {
        vista.txtNombreMaterial.setText("");
        vista.comboCalidad.setSelectedIndex(-1);
        vista.rBtnSi.setSelected(false);
        vista.rBtnNo.setSelected(false);
        vista.txtProcedencia.setText("");
    }

    private boolean comprobarCamposInstrumento() {
        return !vista.txtCodigo.getText().isEmpty() &&
                !(vista.comboTipo.getSelectedIndex() == -1) &&
                !vista.txtNombreInstrumento.getText().isEmpty() &&
                !vista.txtPvp.getText().isEmpty() &&
                !vista.datePicker.getText().isEmpty();
    }

    private boolean comprobarCamposMarca() {
        return !vista.txtNombreMarca.getText().isEmpty() &&
                !(vista.comboPais.getSelectedIndex() == -1) &&
                !vista.txtDelegacion.getText().isEmpty() &&
                !vista.txtWeb.getText().isEmpty();
    }

    private boolean comprobarCamposMaterial() {
        return vista.txtNombreMaterial.getText().isEmpty() ||
                vista.comboCalidad.getSelectedIndex() == -1 ||
                (!vista.rBtnNo.isSelected() && !vista.rBtnSi.isSelected()) ||
                vista.txtProcedencia.getText().isEmpty();
    }



    private void listarInstrumentosBusqueda(ArrayList<Instrumento> lista) {
        vista.dlmInstrumentosBuscar.clear();
        for (Instrumento instrumento : lista) {
            vista.dlmInstrumentosBuscar.addElement(instrumento);
        }
    }

    private void listarMarcasBusqueda(ArrayList<Marca> lista) {
        vista.dlmMarcas.clear();
        for (Marca marca : lista) {
            vista.dlmMarcas.addElement(marca);
        }
    }

    private void listarmaterialesBusqueda(ArrayList<Material> lista) {
        vista.dlmMaterialesBuscar.clear();
        for (Material material : lista) {
            vista.dlmMaterialesBuscar.addElement(material);
        }
    }
    private void listarMateriales() {
        vista.dlmMateriales.clear();
        for (Material material : modelo.getMateriales()) {
            vista.dlmMateriales.addElement(material);
        }
    }


    private void listarMarcas() {
        vista.dlmMarcas.clear();
        for (Marca marca : modelo.getMarcas()) {
            vista.dlmMarcas.addElement(marca);
        }
    }

    private void listarInstrumentos() {
        vista.dlmInstrumentos.clear();
        for (Instrumento instrumento : modelo.getInstrumentos()) {
            vista.dlmInstrumentos.addElement(instrumento);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarInstrumento) {
            listarInstrumentosBusqueda(modelo.getInstrumentos(vista.txtBuscarInstrumento.getText()));
            if (vista.txtBuscarInstrumento.getText().isEmpty()) {
                vista.dlmInstrumentosBuscar.clear();
            }
        } else if (e.getSource() == vista.txtBuscarMarca) {
            listarMarcasBusqueda(modelo.getMarcas(vista.txtBuscarMarca.getText()));
            if (vista.txtBuscarMarca.getText().isEmpty()) {
                vista.dlmMarcasBuscar.clear();
            }
        } else if (e.getSource() == vista.txtBuscarMaterial) {
            listarmaterialesBusqueda(modelo.getMateriales(vista.txtBuscarMaterial.getText()));
            if (vista.txtBuscarMaterial.getText().isEmpty()) {
                vista.dlmMaterialesBuscar.clear();
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == vista.listInstrumentos) {
            if (vista.listInstrumentos.getSelectedValue() != null) {
                Instrumento instrumento = (Instrumento) vista.listInstrumentos.getSelectedValue();
                vista.txtCodigo.setText(instrumento.getCodigo());
                vista.comboTipo.setSelectedItem(instrumento.getTipo());
                vista.txtNombreInstrumento.setText(instrumento.getNombre());
                vista.txtPvp.setText(String.valueOf(instrumento.getPvp()));
                vista.datePicker.setDate(instrumento.getFecha());
            }
        } else if (e.getSource() == vista.listMarcas) {
            if (vista.listMarcas.getSelectedValue() != null) {
                Marca marca = (Marca) vista.listMarcas.getSelectedValue();
                vista.txtNombreMarca.setText(marca.getNombre());
                vista.comboPais.setSelectedItem(marca.getPais());
                vista.txtDelegacion.setText(marca.getDelegacion());
                vista.txtWeb.setText(marca.getWeb());
            }

        } else if (e.getSource() == vista.listMaterial) {
            if (vista.listMaterial.getSelectedValue() != null) {
                Material material = (Material) vista.listMaterial.getSelectedValue();
                vista.txtNombreMaterial.setText(material.getNombre());
                vista.comboCalidad.setSelectedItem(material.getCalidad());
                if (vista.rBtnSi.isSelected()) {
                    vista.organico = material.getOrganico();
                }
                if (vista.rBtnNo.isSelected()) {
                    vista.organico = material.getOrganico();
                }
                vista.txtProcedencia.setText(material.getProcedencia());
            }
        }
    }
}

