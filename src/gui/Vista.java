package gui;

import base.Instrumento;
import base.Marca;
import base.Material;
import com.github.lgooddatepicker.components.DatePicker;
import enums.Calidades;
import enums.Paises;
import enums.TipoInstrumentos;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame{
    private JPanel panel1;
    JTabbedPane tabbedPane1;
    JTextField txtCodigo;
    JComboBox comboTipo;
    JTextField txtNombreInstrumento;
    JTextField txtPvp;
    JButton addInstrumentoBtn;
    JButton modificarInstrumentoBtn;
    JButton eliminarInstrumentoBtn;
    JList listInstrumentos;
    JTextField txtBuscarInstrumento;
    JList listBuscarInstrumento;
    JTextField txtNombreMarca;
    JComboBox comboPais;
    JTextField txtDelegacion;
    JTextField txtWeb;
    JButton addMarcaBtn;
    JButton modificarMarcaBtn;
    JButton eliminarMarcaBtn;
    JList listMarcas;
    JList listBuscarMarcas;
    JTextField txtBuscarMarca;
    JTextField txtNombreMaterial;
    JComboBox comboCalidad;
    JRadioButton rBtnSi;
    JRadioButton rBtnNo;
    JTextField txtProcedencia;
    JButton addMaterialBtn;
    JButton modificarMaterialBtn;
    JButton eliminarMaterialBtn;
    JList listMaterial;
    JTextField txtBuscarMaterial;
    JList listaBuscaarMaterial;
    DatePicker datePicker;
    String organico;

    public String getOrganico() {
        return organico;
    }

    public void setOrganico(String organico) {
        this.organico = organico;
    }

    DefaultListModel<Instrumento> dlmInstrumentos;
    DefaultListModel<Marca> dlmMarcas;
    DefaultListModel<Material> dlmMateriales;
    DefaultListModel<Instrumento> dlmInstrumentosBuscar;
    DefaultListModel<Marca> dlmMarcasBuscar;
    DefaultListModel<Material> dlmMaterialesBuscar;

    JMenuItem itemConectar;
    JMenuItem itemSalir;

    public Vista() {
        setTitle("Instrumentos");
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(800, 650));
        setResizable(false);
        pack();
        setVisible(true);

        inicializarModelos();
        menu();
        setEnumCombo();
    }
    private void setEnumCombo() {
        for (Calidades constant : Calidades.values()) {
            comboCalidad.addItem(constant.getValor());
        }
        comboCalidad.setSelectedIndex(-1);
        for (Paises constant : Paises.values()) {
            comboPais.addItem(constant.getValor());
        }
        comboPais.setSelectedIndex(-1);

        for (TipoInstrumentos constant : TipoInstrumentos.values()) {
            comboTipo.addItem(constant.getValor());
        }
        comboTipo.setSelectedItem(-1);
    }

    private void menu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuArchivo);

        setJMenuBar(menuBar);
    }

    private void inicializarModelos() {
        dlmInstrumentos = new DefaultListModel<>();
        listInstrumentos.setModel(dlmInstrumentos);
        dlmMarcas = new DefaultListModel<>();
        listMarcas.setModel(dlmMarcas);
        dlmMateriales = new DefaultListModel<>();
        listMaterial.setModel(dlmMateriales);
        dlmInstrumentosBuscar = new DefaultListModel<>();
        listBuscarInstrumento.setModel(dlmInstrumentosBuscar);
        dlmMarcasBuscar = new DefaultListModel<>();
        listBuscarMarcas.setModel(dlmMarcasBuscar);
        dlmMaterialesBuscar = new DefaultListModel<>();
        listaBuscaarMaterial.setModel(dlmMaterialesBuscar);
    }
}
